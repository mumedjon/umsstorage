﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMSStorage
{
    class Debit : Product
    {
        public int dt_id { get; set; }
        public int dt_amt { get; set; }
        public double dt_price { get; set; }
        public DateTime dt_date { get; set; }
    }
}
