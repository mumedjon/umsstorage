﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMSStorage
{
    class Product
    {
        public int prod_id { get; set; }
        public string prod_name { get; set; }
        public string prod_model { get; set; }
        public string category { get; set; }
        public string color { get; set; }
        public string country { get; set; }
        //public int status { get; set; }
    }
}
