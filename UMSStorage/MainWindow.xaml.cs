﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UMSStorage
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            initCategories();
            connectToDB();
        }

        private void connectToDB()
        {
        }

        private void initCategories()
        {
            //TODO: get categories from db

            crCBCat.Items.Add("salom");
            crCBCat.Items.Add("v.salom");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //string cstr = @"Data source =.\UMLIMITI; INITIAL Catalog = Mobilephone ; Integrated Security = True";

            //string zap = "SELECT p.prod_id , p.prod_name , p.prod_model , cat.cat_name , col.col_name , ctr.ctr_name , p.status FROM Product as p inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id inner join Country as ctr on p.ctr_id = ctr.ctr_id";

            //using (SqlConnection con = new SqlConnection(cstr))
            //{

            //    con.Open();
            //    SqlCommand com = new SqlCommand(zap, con);
            //    SqlDataReader dr = com.ExecuteReader();
            //    List<Product> products = new List<Product>();
            //    while (dr.Read())
            //    {
            //        Product prod = new Product();
            //        //prod.prod_id = Convert.ToInt32(dr["prod_id"]);
            //        prod.prod_name = Convert.ToString(dr["prod_name"]);
            //        prod.prod_model = Convert.ToString(dr["prod_model"]);
            //        prod.category = Convert.ToString(dr["cat_name"]);
            //        prod.color = Convert.ToString(dr["col_name"]);
            //        prod.country = Convert.ToString(dr["ctr_name"]);
            //        //prod.status = Convert.ToInt32(dr["status"]);
            //        products.Add(prod);
            //    }

            //    //datafield.ItemsSource = products;
            //}
        }

        private void ProgressBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

        private void Credit_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {


        }



        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void crcalc_Click(object sender, RoutedEventArgs e)
        {
            //string crstr = @"Data source =.\UMLIMITI; INITIAL Catalog = Mobilephone ; Integrated Security = True";
            //string zap = "SELECT cr.cr_id , cr.cr_amt , cr.cr_date , cr.cr_price , cr.cr_pricep , p.prod_name , p.prod_model , ctr.ctr_name , cat.cat_name , col.col_name , p.status FROM Credit as cr inner join Product as p on cr.prod_id = p.prod_id inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id inner join Country as ctr on p.ctr_id = ctr.ctr_id";
            //using (SqlConnection connect = new SqlConnection(crstr))
            //{
            //    connect.Open();
            //    List<Credit> credits = new List<Credit>();
            //    SqlCommand command = new SqlCommand(zap, connect);
            //    SqlDataReader dr = command.ExecuteReader();
            //    if (dr.HasRows)
            //    {
            //        while (dr.Read())
            //        {
            //            Credit cr = new Credit();
            //            cr.cr_id = Convert.ToInt32(dr["cr_id"]);
            //            cr.cr_amt = Convert.ToInt32(dr["cr_amt"]);
            //            cr.cr_date = Convert.ToDateTime(dr["cr_date"]);
            //            cr.cr_price = Convert.ToDouble(dr["cr_price"]);
            //            cr.cr_pricep = Convert.ToDouble(dr["cr_pricep"]);
            //            cr.prod_name = Convert.ToString(dr["prod_name"]);
            //            cr.prod_model = Convert.ToString(dr["prod_model"]);
            //            cr.category = Convert.ToString(dr["cat_name"]);
            //            cr.color = Convert.ToString(dr["col_name"]);
            //            cr.country = Convert.ToString(dr["ctr_name"]);
            //            credits.Add(cr);

            //        }
            //        crdata.ItemsSource = credits;
            //    }
            //    else
            //    {
            //        MessageBox.Show("Пусто");
            //    }
            //}
        }

        private void cradd_Click(object sender, RoutedEventArgs e)
        {
            if (isCorrectTextBoxTexts())
            {

                //string crstr = @"Data Source =.\UMLIMITI ; INITIAL Catalog = Mobilephone ; Integrated Security = True";
                //using (SqlConnection con = new SqlConnection(crstr))
                //{
                //    con.Open();
                //    Credit cr = new Credit();
                //    cr.prod_name = crname.Text;
                //    cr.prod_model = crmodel.Text;
                //    cr.cr_amt = Convert.ToInt32(cramt.Text);
                //    // TODO: categoriya az rui conbobox girifta shavad 
                //    //cr.category = crcat.Text;
                //    cr.cr_price = Convert.ToDouble(crprice1.Text);
                //    cr.cr_pricep = Convert.ToDouble(crprice2.Text);
                //    cr.color = crcol.Text;
                //    cr.country = crctr.Text;
                //    string zap = string.Format("INSERT INTO Product (prod_name, prod_model, ctr_id, cat_id, col_id, status) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}' , 1)", cr.prod_name, cr.prod_model, cr.country, cr.category, cr.color);
                //    SqlCommand com = new SqlCommand(zap, con);
                //    SqlDataReader dr = com.ExecuteReader();
                //    //cr.prod_id = Convert.ToInt32(dr["prod_id"]);

                //    string zap2 = string.Format("INSERT INTO Credit (cr_amt, cr_price, cr_pricep, prod_id) VALUES ('{0}', '{1}', '{2}', '{3}')", cr.cr_amt, cr.cr_price, cr.cr_pricep);

                //}
            }
        }

        /// <summary>
        /// Функсия барои тафтиши додаҳои текстбоксҳо.
        /// Ҳангоми дуруст будани додаҳо true онгоҳ false бармегардонад.
        /// </summary>
        /// <returns>boolean</returns>
        private bool isCorrectTextBoxTexts()
        {
            bool hasError = false;
            string errorText = "";
            if (!(cramt.Text.Length > 0 && cramt.Text.Length < 5))
            {
                // TODO: оиди хатогӣ маълумоти пурра чоп карда шавад. Инчунин тарзи ислоҳи ин хатогӣ нишон дода шавад.
                errorText += "Поля количество не число.\n";
                hasError = true;
            }
            if (crname.Text.Equals(""))
            {
                hasError = true;
                errorText += "Наименованые продукта пусто.\n";
            }
            if (crmodel.Text.Equals(""))
            {
                hasError = true;
                errorText += "Укажите модель продукта.\n";
            }
            if (!(crprice1.Text.Length > 0 && crprice1.Text.Length < 5))
            {
                hasError = true;
                errorText += "Укажите цену.\n";
            }
            if (!(crprice2.Text.Length > 0 && crprice2.Text.Length < 5))
            {
                hasError = true;
                errorText += "Укажите кон. цену.\n";
            }

            if (hasError)
            {
                MessageBox.Show(errorText, "Error - SStorage", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else
            {
                // hamai dodahoi textboxho durust
                return true;
            }
        }

        private void button_Copy3_Click(object sender, RoutedEventArgs e)
        {
            //string dbstr = @"Data Source =.\UMLIMITI ; INITIAL Catalog = Mobilephone ; Integrated Security = True";
            //string zap = "SELECT dt.dt_id , dt.dt_amt , dt.dt_date , dt.dt_price , p.prod_id , p.prod_name , p.prod_model , ctr.ctr_name , cat.cat_name , col.col_name , p.status FROM Debit as dt inner join Product as p on dt.prod_id = p.prod_id inner join Country as ctr on p.ctr_id = ctr.ctr_id inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id";
            //List<Debit> debit = new List<Debit>();
            //using (SqlConnection con = new SqlConnection(dbstr))
            //{
            //    con.Open();
            //    SqlCommand command = new SqlCommand(zap, con);
            //    SqlDataReader dr = command.ExecuteReader();
            //    if (dr.HasRows)
            //    {
            //        //new push
            //        while (dr.Read())
            //        {
            //            Debit dt = new Debit();
            //            dt.dt_id = Convert.ToInt32(dr["dt_id"]);
            //            dt.dt_amt = Convert.ToInt32(dr["dt_amt"]);
            //            dt.dt_date = Convert.ToDateTime(dr["dt_date"]);
            //            dt.dt_price = Convert.ToDouble(dr["dt_price"]);
            //            // dt.prod_id = Convert.ToInt32(dr["prod_id"]);
            //            dt.prod_name = Convert.ToString(dr["prod_name"]);
            //            dt.prod_model = Convert.ToString(dr["prod_model"]);
            //            dt.category = Convert.ToString(dr["cat_name"]);
            //            dt.country = Convert.ToString(dr["ctr_name"]);
            //            dt.color = Convert.ToString(dr["col_name"]);
            //            //dt.status = Convert.ToInt32(dr["status"]);
            //            debit.Add(dt);
            //        }
            //        dtdata.ItemsSource = debit;
            //    }
            //    else
            //    {
            //        MessageBox.Show("Пусто");
            //    }
            //}
        }

        private void crfind_Copy_Click(object sender, RoutedEventArgs e)
        {
            //string prstr = @"Data Source =.\UMLIMITI ; INITIAL Catalog = Mobilephone ; Integrated Security = True";
            //using (SqlConnection con = new SqlConnection(prstr))
            //{
            //    string zap;
            //    con.Open();
            //    if (prname.Text == "" && prmodel.Text == "")
            //    {
            //        zap = "SELECT p.status, p.prod_name, p.prod_model, ctr.ctr_name, cat.cat_name, col.col_name FROM Product as p inner join Country as ctr on p.ctr_id = ctr.ctr_id inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id WHERE p.status = 1";
            //    }
            //    else
            //    {
            //        if (prmodel.Text == "" && prname.Text != "")
            //        {
            //            zap = string.Format("SELECT p.status, p.prod_name, p.prod_model, ctr.ctr_name, cat.cat_name, col.col_name FROM Product as p inner join Country as ctr on p.ctr_id = ctr.ctr_id inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id WHERE p.status = 1 AND p.prod_name = '{0}'", prname.Text);
            //        }
            //        else
            //        {
            //            zap = string.Format("SELECT p.status, p.prod_name, p.prod_model, ctr.ctr_name, cat.cat_name, col.col_name FROM Product as p inner join Country as ctr on p.ctr_id = ctr.ctr_id inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id WHERE p.status = 1 AND p.prod_model = '{0}'", prmodel.Text);
            //        }

            //    }
            //    if (prname.Text != "" && prmodel.Text != "")
            //    {
            //        zap = string.Format("SELECT p.prod_name, p.prod_model, ctr.ctr_name, cat.cat_name, col.col_name, p.status FROM Product as p inner join Country as ctr on p.ctr_id = ctr.ctr_id inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id WHERE p.prod_name = '{1}' AND p.prod_model = '{0}'", prmodel.Text, prname.Text);
            //    }
            //    SqlCommand com = new SqlCommand(zap, con);
            //    com.ExecuteNonQuery();
            //    SqlDataReader dr = com.ExecuteReader();
            //    List<Product> products = new List<Product>();
            //    while (dr.Read())
            //    {
            //        Product prod = new Product();
            //        //prod.prod_id = Convert.ToInt32(dr["prod_id"]);
            //        prod.prod_name = Convert.ToString(dr["prod_name"]);
            //        prod.prod_model = Convert.ToString(dr["prod_model"]);
            //        prod.category = Convert.ToString(dr["cat_name"]);
            //        prod.color = Convert.ToString(dr["col_name"]);
            //        prod.country = Convert.ToString(dr["ctr_name"]);
            //        //prod.status = Convert.ToInt32(dr["status"]);
            //        products.Add(prod);
            //    }
            //    prdata.ItemsSource = products;
            //}
        }

        private void prdel_Click(object sender, RoutedEventArgs e)
        {
            //string dprstr = @"Data Source =.\UMLIMITI ; INITIAL Catalog = Mobilephone ; Integrated Security = True";
            //using (SqlConnection con = new SqlConnection(dprstr))
            //{
            //    string zap;
            //    con.Open();

            //    if (prname.Text == "" && prmodel.Text == "")
            //    {
            //        MessageBox.Show("Ошибка. Все поля пусты!");
            //        //zap = "str";
            //    }
            //    else
            //    {
            //        if (prmodel.Text == "" && prname.Text != "")
            //        {
            //            zap = string.Format("UPDATE Product SET status = 0 WHERE prod_name = '{0}'", prname.Text);
            //            SqlCommand com = new SqlCommand(zap, con);
            //            com.ExecuteNonQuery();
            //        }
            //        else
            //        {
            //            zap = string.Format("UPDATE Product SET status = 0 WHERE prod_model = '{0}'", prmodel.Text);
            //            SqlCommand com = new SqlCommand(zap, con);
            //            com.ExecuteNonQuery();
            //        }
            //    }
            //    string zapp = "SELECT p.prod_id , p.prod_name , p.prod_model , cat.cat_name , col.col_name , ctr.ctr_name , p.status FROM Product as p inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id inner join Country as ctr on p.ctr_id = ctr.ctr_id";

            //    using (SqlConnection conn = new SqlConnection(dprstr))
            //    {

            //        conn.Open();
            //        SqlCommand com = new SqlCommand(zapp, con);
            //        SqlDataReader dr = com.ExecuteReader();
            //        List<Product> products = new List<Product>();
            //        while (dr.Read())
            //        {
            //            Product prod = new Product();
            //            //prod.prod_id = Convert.ToInt32(dr["prod_id"]);
            //            prod.prod_name = Convert.ToString(dr["prod_name"]);
            //            prod.prod_model = Convert.ToString(dr["prod_model"]);
            //            prod.category = Convert.ToString(dr["cat_name"]);
            //            prod.color = Convert.ToString(dr["col_name"]);
            //            prod.country = Convert.ToString(dr["ctr_name"]);
            //            //prod.status = Convert.ToInt32(dr["status"]);
            //            products.Add(prod);
            //        }
            //        conn.Close();
            //        //datafield.ItemsSource = products;
            //    }

            //    //SqlCommand com = new SqlCommand(zap , con);

            //}
        }

        private void crfind_Click(object sender, RoutedEventArgs e)
        {
            //    string fcstr = @"Data Source =.\UMLIMITI ; INITIAL Catalog = Mobilephone; Integrated Security = True";
            //    using (SqlConnection con = new SqlConnection(fcstr))
            //    {
            //        string zap;
            //        con.Open();
            //        if (crname.Text == "" && crmodel.Text == "")
            //        {
            //            zap = "SELECT cr.cr_id , cr.cr_amt , cr.cr_date , cr.cr_price , cr.cr_pricep , p.prod_name , p.prod_model , ctr.ctr_name , cat.cat_name , col.col_name , p.status FROM Credit as cr inner join Product as p on cr.prod_id = p.prod_id inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id inner join Country as ctr on p.ctr_id = ctr.ctr_id WHERE p.status = 1";
            //        }
            //        else
            //        {
            //            if (crmodel.Text == "" && crname.Text != "")
            //            {
            //                zap = string.Format("SELECT cr.cr_id , cr.cr_amt , cr.cr_date , cr.cr_price , cr.cr_pricep , p.prod_name , p.prod_model , ctr.ctr_name , cat.cat_name , col.col_name , p.status FROM Credit as cr inner join Product as p on cr.prod_id = p.prod_id inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id inner join Country as ctr on p.ctr_id = ctr.ctr_id WHERE p.status = 1 AND p.prod_name = '{0}'", crname.Text);
            //            }
            //            else
            //            {
            //                zap = string.Format("SELECT cr.cr_id , cr.cr_amt , cr.cr_date , cr.cr_price , cr.cr_pricep , p.prod_name , p.prod_model , ctr.ctr_name , cat.cat_name , col.col_name , p.status FROM Credit as cr inner join Product as p on cr.prod_id = p.prod_id inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id inner join Country as ctr on p.ctr_id = ctr.ctr_id WHERE p.status = 1 AND p.prod_model = '{0}'", crmodel.Text);
            //            }

            //        }
            //        if (crname.Text != "" && crmodel.Text != "")
            //        {
            //            zap = string.Format("SELECT cr.cr_id, cr.cr_amt, cr.cr_date, cr.cr_price, cr.cr_pricep, p.prod_name, p.prod_model, ctr.ctr_name, cat.cat_name, col.col_name, p.status FROM Credit as cr inner join Product as p on cr.prod_id = p.prod_id inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id inner join Country as ctr on p.ctr_id = ctr.ctr_id WHERE p.status = 1 AND p.prod_model = '{0} AND p.prod_name = '{1}'", crmodel.Text, crname.Text);
            //        }
            //        //string zap = "SELECT cr.cr_id , cr.cr_amt , cr.cr_date , cr.cr_price , cr.cr_pricep , p.prod_name , p.prod_model , ctr.ctr_name , cat.cat_name , col.col_name , p.status FROM Credit as cr inner join Product as p on cr.prod_id = p.prod_id inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id inner join Country as ctr on p.ctr_id = ctr.ctr_id";
            //        List<Credit> credits = new List<Credit>();
            //        SqlCommand command = new SqlCommand(zap, con);
            //        SqlDataReader dr = command.ExecuteReader();
            //        if (dr.HasRows)
            //        {
            //            while (dr.Read())
            //            {
            //                Credit cr = new Credit();
            //                cr.cr_id = Convert.ToInt32(dr["cr_id"]);
            //                cr.cr_amt = Convert.ToInt32(dr["cr_amt"]);
            //                cr.cr_date = Convert.ToDateTime(dr["cr_date"]);
            //                cr.cr_price = Convert.ToDouble(dr["cr_price"]);
            //                cr.cr_pricep = Convert.ToDouble(dr["cr_pricep"]);
            //                cr.prod_name = Convert.ToString(dr["prod_name"]);
            //                cr.prod_model = Convert.ToString(dr["prod_model"]);
            //                cr.category = Convert.ToString(dr["cat_name"]);
            //                cr.color = Convert.ToString(dr["col_name"]);
            //                cr.country = Convert.ToString(dr["ctr_name"]);
            //                credits.Add(cr);

            //            }
            //            crdata.ItemsSource = credits;
            //        }
            //        else
            //        {
            //            MessageBox.Show("Пусто");
            //        }
            //    }
        }

        private void dtfind_Click(object sender, RoutedEventArgs e)
        {
            //string fcstr = @"Data Source =.\UMLIMITI ; INITIAL Catalog = Mobilephone; Integrated Security = True";
            //using (SqlConnection con = new SqlConnection(fcstr))
            //{
            //    string zap;
            //    con.Open();
            //    if (dtname.Text == "" && dtmodel.Text == "")
            //    {
            //        zap = "SELECT dt.dt_id , dt.dt_amt , dt.dt_date , dt.dt_price , p.prod_name , p.prod_model , ctr.ctr_name , cat.cat_name , col.col_name , p.status FROM Debit as dt inner join Product as p on dt.prod_id = p.prod_id inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id inner join Country as ctr on p.ctr_id = ctr.ctr_id WHERE p.status = 1";
            //    }
            //    else
            //    {
            //        if (dtmodel.Text == "" && dtname.Text != "")
            //        {
            //            zap = string.Format("SELECT dt.dt_id , dt.dt_amt , dt.dt_date , dt.dt_price , p.prod_name , p.prod_model , ctr.ctr_name , cat.cat_name , col.col_name , p.status FROM Debit as dt inner join Product as p on dt.prod_id = p.prod_id inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id inner join Country as ctr on p.ctr_id = ctr.ctr_id WHERE p.status = 1 AND p.prod_name = '{0}'", dtname.Text);
            //        }
            //        else
            //        {
            //            zap = string.Format("SELECT dt.dt_id , dt.dt_amt , dt.dt_date , dt.dt_price , p.prod_name , p.prod_model , ctr.ctr_name , cat.cat_name , col.col_name , p.status FROM Debit as dt inner join Product as p on dt.prod_id = p.prod_id inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id inner join Country as ctr on p.ctr_id = ctr.ctr_id WHERE p.status = 1 AND p.prod_model = '{0}'", dtmodel.Text);
            //        }

            //    }
            //    if (dtname.Text != "" && dtmodel.Text != "")
            //    {
            //        zap = string.Format("SELECT dt.dt_id , dt.dt_amt , dt.dt_date , dt.dt_price , p.prod_name , p.prod_model , ctr.ctr_name , cat.cat_name , col.col_name , p.status FROM Debit as dt inner join Product as p on dt.prod_id = p.prod_id inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id inner join Country as ctr on p.ctr_id = ctr.ctr_id WHERE p.status = 1 AND p.prod_model = '{0} AND p.prod_name = '{1}'", dtmodel.Text, dtname.Text);
            //    }
            //    //string zap = "SELECT cr.cr_id , cr.cr_amt , cr.cr_date , cr.cr_price , cr.cr_pricep , p.prod_name , p.prod_model , ctr.ctr_name , cat.cat_name , col.col_name , p.status FROM Credit as cr inner join Product as p on cr.prod_id = p.prod_id inner join Category as cat on p.cat_id = cat.cat_id inner join Color as col on p.col_id = col.col_id inner join Country as ctr on p.ctr_id = ctr.ctr_id";
            //    List<Debit> debits = new List<Debit>();
            //    SqlCommand command = new SqlCommand(zap, con);
            //    SqlDataReader dr = command.ExecuteReader();
            //    if (dr.HasRows)
            //    {
            //        while (dr.Read())
            //        {
            //            Debit dt = new Debit();
            //            dt.dt_id = Convert.ToInt32(dr["dt_id"]);
            //            dt.dt_amt = Convert.ToInt32(dr["dt_amt"]);
            //            dt.dt_date = Convert.ToDateTime(dr["dt_date"]);
            //            dt.dt_price = Convert.ToDouble(dr["dt_price"]);
            //            dt.prod_name = Convert.ToString(dr["prod_name"]);
            //            dt.prod_model = Convert.ToString(dr["prod_model"]);
            //            dt.category = Convert.ToString(dr["cat_name"]);
            //            dt.color = Convert.ToString(dr["col_name"]);
            //            dt.country = Convert.ToString(dr["ctr_name"]);
            //            debits.Add(dt);

            //        }
            //        dtdata.ItemsSource = debits;
            //    }
            //    else
            //    {
            //        MessageBox.Show("Пусто");
            //        dtdata.ItemsSource = null;
            //    }
            //}
        }

        private void cramt_KeyDown(object sender, KeyEventArgs e)
        {
            isKeyDownNumber(sender, e);
        }

        private void isKeyDownNumber(object sender, KeyEventArgs e)
        {
            string number;
            if ((e.Key >= Key.D0 && e.Key <= Key.D9))
            {
                //digit pressed!

            }
            else if (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
            {
                // blablabla
            }
            else
            {
                //no digit...
                e.Handled = true;
            }
        }

        private void crprice1_KeyDown(object sender, KeyEventArgs e)
        {
            isKeyDownNumber(sender, e);
        }

        private void crprice2_KeyDown(object sender, KeyEventArgs e)
        {
            isKeyDownNumber(sender, e);
        }


        //validating




    }


}
