﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UMSStorage
{
    /// <summary>
    /// Логика взаимодействия для Auth.xaml
    /// </summary>
    public partial class Auth : Window
    {
        public Auth()
        {
            InitializeComponent();

            refresh();
            this.GotFocus += (sender, args) =>
            {
                //your code here
                refreshAfterIncorrectData(sender);
            };
        }

        private void refreshAfterIncorrectData(object sender)
        {
            if (lb_incorrect.Visibility == Visibility.Visible)
            {
                refresh();

            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            checkLoginAndPassword();
        }

        private void checkLoginAndPassword()
        {
            if (pb_password.Password.Equals("warning"))
            {
                MessageBox.Show("rahmat");
            }
            if (tb_login.Text.Equals("admin") && pb_password.Password.ToString().Equals("password"))
            {
                //login and password correct
                openMainWindow();
            }
            else
            {
                redView();
            }

        }

        private void redView()
        {
            lb_incorrect.Visibility = Visibility.Visible;
            pb_password.Password = "";
            tb_login.Text = "";
            tb_login.BorderBrush = Brushes.Red;
            pb_password.BorderBrush = Brushes.Red;
        }

        private void refresh()
        {
            lb_incorrect.Visibility = Visibility.Hidden;
            pb_password.Password = "";
            tb_login.Text = "";
            pb_password.BorderBrush = Brushes.Black;
            tb_login.BorderBrush = Brushes.Black;

        }
        private void openMainWindow()
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }

        private void pb_password_KeyDown(object sender, KeyEventArgs e)
        {

        }
    }
}
