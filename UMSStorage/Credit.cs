﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMSStorage
{
    class Credit : Product
    {

        public int cr_id { get; set; }
        public int cr_amt { get; set; }
        public DateTime cr_date { get; set; }
        public double cr_price { get; set; }
        public double cr_pricep { get; set; }

    }
}
