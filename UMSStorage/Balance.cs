﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMSStorage
{
    class Balance
    {
        public string name { get; set; }
        public string model { get; set; }
        public string category { get; set; }
        public double price { get; set; }
        public int amount { get; set; }
    }
}
